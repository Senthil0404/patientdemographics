using RestApi.Repository;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace RestApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IPatientDemographicsRepository, PatientDemographicRepository>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}