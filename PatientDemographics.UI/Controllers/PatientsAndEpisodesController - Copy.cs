﻿using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace PatientAndEpisodes.Controllers
{
    public class PatientsAndEpisodesController : Controller
    {
        public ActionResult GetAllPatient()
        {
            return View();
        }

        // GET: Home  
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult EmpDetails()
        {
            //IEnumerable<Patient> students = null;

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:56479/patients");
            //    //HTTP GET
            //    var responseTask = client.GetAsync("2/episodes");
            //    responseTask.Wait();

            //    var result = responseTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        var readTask = result.Content.ReadAsStringAsync();
            //        readTask.Wait();

            //       // students = readTask;
            //    }
            //    else //web api sent error response 
            //    {
            //        //log response status here..

            //        students = Enumerable.Empty<Patient>();

            //        ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            //    }
            //}

            Patient Objemployee = new Patient();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:56479/patients/");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("2/episodes").Result;
            if (response.IsSuccessStatusCode)
            {
                var Patient = response.Content.ReadAsStringAsync().Result;
            }

            var client1 = new RestClient("http://localhost:56479/patients");
            RestRequest request = new RestRequest("2/episodes", Method.GET);
            request.AddHeader("User-Agent", "Nothing");

            // Execute the request and automatically deserialize the result.
            var contributors = client1.Execute<List<Patient>>(request);

            ////IRestResponse<Patient> response = client.Execute<Patient>(request);
            //var response1 = client.Execute(request);
            //  Patient myDeserializedObj = (Patient)JavaScriptConvert.DeserializeObject(response1.Content, typeof(Patient));

            //RestSharp.Deserializers.JsonDeserializer deserial = new JsonDeserializer();
            //Patient people = deserial.Deserialize<Patient>(response1 as RestResponse);

            //People people = deserial.Deserialize<People>(response);
            //return response.Data;

            //Creating List      
            List<Employee> ObjEmp = new List<Employee>()
            {


                //Adding records to list      
                new Employee
                {
                    Id = 1, Name = "Vithal Wadje", City = "Latur", Address = "Kabansangvi"
                },
                new Employee
                {
                    Id = 2, Name = "Sudhir Wadje", City = "Mumbai", Address = "Kurla"
                },
                 new Employee
                {
                    Id = 3, Name = "Dinesh Beniwal", City = "New Delhi", Address = "Noida"
                },
                   new Employee
                {
                    Id = 4, Name = "Dhananjay Kumar", City = "New Delhi", Address = "Delhi"
                },
                     new Employee
                {
                    Id = 5, Name = "Jitendra Gund", City = "Pune", Address = "Pune"
                },
                       new Employee
                {
                    Id = 6, Name = "Anil Kumar", City = "chandigarh", Address = "chandigarh"
                },
                         new Employee
                {
                    Id = 7, Name = "Ramesh", City = "Mumbai", Address = "Kurla"
                },
                           new Employee
                {
                    Id = 8, Name = "Sachin", City = "XYZ", Address = "XYZ"
                },
                                        new Employee
                {
                    Id = 9, Name = "Ravi", City = "XYZ", Address = "XYZ"
                },
                                                     new Employee
                {
                    Id = 10, Name = "Kapil", City = "XYZ", Address = "XYZ"
                },
                                                                  new Employee
                {
                    Id = 11, Name = "Vivek", City = "XYZ", Address = "XYZ"
                }
            };
            //return response.Content      
            return Json(ObjEmp, JsonRequestBehavior.AllowGet);
        }
    }

    public class Patient
    {
        public int PatientId { get; set; }

        public string NhsNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public IEnumerable<Episode> Episodes { get; set; }
    }

    public class Episode
    {
        public int EpisodeId { get; set; }

        public int PatientId { get; set; }

        public DateTime AdmissionDate { get; set; }

        public DateTime DischargeDate { get; set; }

        public string Diagnosis { get; set; }
    }

    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
    }
}

