﻿using PatientDemographicsRepository.Models;

namespace RestApi.Repository
{
    public interface IPatientDemographicsRepository
    {
        PatientPagingModel GetAllPatientDemographics(int currentPage, int recordsPerPage);
        bool PostPatientDetails(PatientDetail patientViewModel);
    }
}
