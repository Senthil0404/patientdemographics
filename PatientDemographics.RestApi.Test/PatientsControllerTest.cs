﻿using Moq;
using NUnit.Framework;
using PatientDemographicsRepository.Models;
using RestApi.Controllers;
using RestApi.Repository;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;

namespace RestApi.Test
{
    [TestFixture]
    public class PatientsControllerTest
    {
        IPatientDemographicsRepository _PatientDemographicsRepository;
        PatientDetail patientDetail = null;
        [SetUp]
        public void Setup()
        {
            patientDetail = new PatientDetail() { ForeName = "Senthil", SurName = "Kumar", Gender = "Male" };

            List<PatientDetail> lstPatient = new List<PatientDetail>();
            lstPatient.Add(new PatientDetail() { ForeName = "Senthil", SurName = "Kumar", DateofBirth = DateTime.Now.AddDays(-20), Gender = "Male" });
            lstPatient.Add(new PatientDetail() { ForeName = "Rohith", SurName = "S", DateofBirth = DateTime.Now.AddDays(-20), Gender = "Male" });
            PatientPagingModel patientPagingModel = new PatientPagingModel() { TotalRecords = 5, PatientDetails = lstPatient };

            ////Used MOQ framework to return predefined  data
            var PatientDemographicRepository = new Mock<IPatientDemographicsRepository>();
            PatientDemographicRepository.Setup(x => x.GetAllPatientDemographics(1,4)).Returns(patientPagingModel);
            PatientDemographicRepository.Setup(x => x.PostPatientDetails(patientDetail)).Returns(true);
            _PatientDemographicsRepository = PatientDemographicRepository.Object;
        }

        [Test]
        public void GetValidPatient()
        {
            IHttpActionResult actionResult = new PatientsController(_PatientDemographicsRepository).Get(1, 4);
            var contentResult = actionResult as OkNegotiatedContentResult<PatientPagingModel>;

            Assert.IsNotNull(contentResult.Content.PatientDetails);
        }

        [Test]
        public void PostPatientDetailSuccess()
        {
            IHttpActionResult actionResult = new PatientsController(_PatientDemographicsRepository).Post(patientDetail);
            var contentResult = actionResult as OkNegotiatedContentResult<bool>;

            Assert.IsTrue(contentResult.Content);
        }
        [Test]
        public void PostPatientDetailFailed()
        {
            patientDetail = new PatientDetail() { ForeName = "Rohith", SurName = "S", Gender = "Male" };
            IHttpActionResult actionResult = new PatientsController(_PatientDemographicsRepository).Post(patientDetail);
            var contentResult = actionResult as OkNegotiatedContentResult<bool>;

            Assert.IsFalse(contentResult.Content);
        }
        [Test]
        public void PostPatientDetailInvalidRequest()
        {
            patientDetail = null;

            HttpResponseException ex = Assert.Throws<HttpResponseException>(() => new PatientsController(_PatientDemographicsRepository).Post(patientDetail));

            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }
    }
}