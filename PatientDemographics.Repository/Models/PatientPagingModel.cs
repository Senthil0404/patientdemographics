﻿using System.Collections.Generic;

namespace PatientDemographicsRepository.Models
{
    public class PatientPagingModel
    {
        public int TotalRecords { get; set; }
        public IEnumerable<PatientDetail> PatientDetails { get; set; }
    }
}