﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace PatientDemographicsRepository.Models
{
    public class PatientDetail
    {
        public string ForeName { get; set; }
        public string SurName { get; set; }

        private DateTime? dob;
        [XmlIgnore]
        public DateTime? DateofBirth
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
            }
        }

        [XmlElement("DateofBirth")]
        public string DoBstring
        {
            get
            {
                return DateofBirth.HasValue
                ? XmlConvert.ToString(DateofBirth.Value, XmlDateTimeSerializationMode.Unspecified)
                : string.Empty;
            }
            set
            {
                DateofBirth =
                !string.IsNullOrEmpty(value)
                ? XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.Unspecified)
                : (DateTime?)null;
            }
        }

        public string Gender { get; set; }
        public PatientTelephoneNumber TelephoneNumbers { get; set; }
    }
}
