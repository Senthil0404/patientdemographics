﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace RestApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute( name: "Default", routeTemplate: "api/{controller}");
            //Enabled cors
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            //EnableCorsAttribute cors = new EnableCorsAttribute("http://localhost", "*", "*");
            config.EnableCors(cors);
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
